#pragma once

#include <vector>

#include "cf_ui/Widget.hpp"
#include "cf_util/Signal.hpp"


class Profile;
class ProfileWidget;
class ProfileSelector;


namespace cf
{
    class Button;
} // namespace cf


class MainMenu : public cf::Widget
{
    cf::Button *_start_game_button = nullptr;
    cf::Button *_options_button = nullptr;

    ProfileWidget *_profile_widget = nullptr;
    ProfileSelector *_profile_selector = nullptr;

    std::vector<std::shared_ptr<Profile>> _all_profiles;
    std::shared_ptr<Profile> _active_profile;

public:
    MainMenu(const cf::SizeI &window_size);

    inline std::shared_ptr<Profile> get_active_profile()
    {
        return _active_profile;
    }

    cf::Signal<> new_game_clicked;
    cf::Signal<> options_clicked;
    cf::Signal<> exit_clicked;

private:
    int index_of_active_profile() const;
    bool is_new_profile_name_valid(const std::string &name) const;

    bool has_previous_profile() const;
    bool has_next_profile() const;

    bool can_start_with_active_profile() const;

    void update_main_widgets();

    void on_next_profile_clicked();
    void on_prev_profile_clicked();
    void on_begin_profile_creation();
    void on_end_profile_creation();
    void on_new_profile_created(std::shared_ptr<Profile> new_profile);
};
