#pragma once

#include "cf_ui/Widget.hpp"
#include "cf_util/Signal.hpp"

#include "game/Spaceship.hpp"


class GameWidget : public cf::Widget
{
    cf::Widget *_pause_menu = nullptr;
    bool _paused = false;

    std::shared_ptr<Spaceship> _player;

public:
    GameWidget(const cf::SizeI &window_size);

    inline void set_player(std::shared_ptr<Spaceship> obj)
    {
        _player = std::move(obj);
    }

    void init();

    void set_paused(bool value);

    cf::Signal<> exit_to_hangar_clicked;

protected:
    void key_press_event(cf::KeyPressEvent *event) override;

    /**
     * @brief Событие отрисовки
     */
    void paint_event() override;
};
