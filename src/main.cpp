#include "cf_ui/Application.hpp"
#include "cf_ui/Window.hpp"

#include "GameWidget.hpp"
#include "MainMenu.hpp"
#include "hangar/HangarWidget.hpp"


namespace
{
    /**
     * @brief Класс текущего приложения
     */
    class SpaceApplication : public cf::Application
    {
        static constexpr const char *app_name = "Space";
        static constexpr cf::SizeI window_size = {1920, 1080};

        MainMenu *_main_menu = nullptr;
        GameWidget *_game_widget = nullptr;
        HangarWidget *_hangar = nullptr;

    public:
        /**
         * @brief Класс текущего приложения
         */
        SpaceApplication()
            : cf::Application(app_name, window_size)
            , _main_menu(new MainMenu(window_size))
            , game_widget(new GameWidget(window_size))
            , hangar(new HangarWidget(window_size))
        {
            window()->add_child(_main_menu);
            window()->add_child(_game_widget);
            window()->add_child(_hangar);

            get_window()->set_keyboard_focus(_game_widget);

            _game_widget->set_visible(false);
            _hangar->set_visible(false);

            _game_widget->exit_to_hangar_clicked.connect(this, &SpaceApplication::on_game_exit_to_hangar_clicked);
            _hangar->exit_to_main.connect(this, &SpaceApplication::on_hangar_exit_to_main_clicked);
            _hangar->start_clicked.connect(this, &SpaceApplication::on_level_start_clicked);

            _main_menu->new_game_clicked.connect(this, &SpaceApplication::on_new_game_clicked);
            _main_menu->options_clicked.connect(this, &SpaceApplication::on_options_clicked);
            _main_menu->exit_clicked.connect(this, &SpaceApplication::on_exit_clicked);
        }

    private:
        void on_level_start_clicked(std::shared_ptr<Spaceship> spaceship, std::shared_ptr<Level> level)
        {
            _game_widget->set_player(std::move(spaceship));
            _game_widget->set_level(std::move(level));

            if (not _game_widget->can_start())
            {
                return;
            }

            _game_widget->init();
            _hangar->set_visible(false);
            _game_widget->set_visible(true);
        }

        /**
         * @brief Нажата кнопка "Выйти в ангар" в игре
         */
        void on_game_exit_to_hangar_clicked()
        {
            _game_widget->set_visible(false);
            _hangar->set_visible(true);
        }

        /**
         * @brief Нажата кнопка "Выйти в главное меню" в ангаре
         */
        void on_hangar_exit_to_main_clicked()
        {
            _hangar->set_visible(false);
            _main_menu->set_visible(true);
        }

        /**
         * @brief Нажата кнопка "Начать игру" (гл. меню)
         */
        void on_new_game_clicked()
        {
            _main_menu->set_visible(false);
            _hangar->set_visible(true);
        }

        /**
         * @brief Нажата кнопка "Настройки" (гл. меню)
         */
        void on_options_clicked()
        {
        }

        /**
         * @brief Нажата кнопка "Выход" (гл. меню)
         */
        void on_exit_clicked()
        {
            quit();
        }
    };
} // namespace


int main()
{
    return SpaceApplication().execute();
}
