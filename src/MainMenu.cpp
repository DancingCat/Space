#include "MainMenu.hpp"

#include <cassert>
#include <filesystem>

#include "cf_ui/Button.hpp"
#include "cf_ui/Painter.hpp"
#include "cf_ui/TickEvent.hpp"

#include "profile/Profile.hpp"
#include "profile/ProfileSelector.hpp"
#include "profile/ProfileWidget.hpp"


namespace
{
    std::vector<std::shared_ptr<Profile>> load_all_profiles()
    {
        std::vector<std::shared_ptr<Profile>> result;

        for (const auto &entry : std::filesystem::directory_iterator(Profile::file_dir))
        {
            if (not entry.is_regular_file() or entry.path().extension() != Profile::file_ext)
            {
                continue;
            }

            auto profile = Profile::load_from_file(entry.path());

            if (profile == nullptr)
            {
                continue;
            }

            result.emplace_back(std::move(profile));
        }

        return result;
    }
} // namespace


MainMenu::MainMenu(const cf::SizeI &window_size)
    : cf::Widget(cf::PointI{}, window_size)
    , _all_profiles(load_all_profiles())
{
    constexpr cf::SizeI button_size = {350, 100};
    constexpr cf::SizeI selector_size = {500, 175};

    constexpr int offset = 150;
    constexpr int profile_offset = 5;

    _start_game_button = new cf::Button(button_size);
    _start_game_button->set_text("Начать игру");
    _start_game_button->clicked.connect(new_game_clicked);
    add_child(_start_game_button);

    _options_button = new cf::Button(button_size);
    _options_button->set_text("Настройки");
    _options_button->clicked.connect(options_clicked);
    add_child(_options_button);

    auto exit = new cf::Button(button_size);
    exit->set_text("Выход");
    exit->clicked.connect(exit_clicked);
    add_child(exit);

    // TODO:
    // layout

    cf::SizeI profile_wgt_size = {selector_size.width(), window_size.height() - profile_offset * 4 - selector_size.height()};

    _profile_widget = new ProfileWidget(cf::RectI(cf::PointI(window_size.width() - profile_wgt_size.width() - profile_offset,
                                                             profile_offset + profile_offset * 2 + selector_size.height()),
                                                  profile_wgt_size),
                                        nullptr);
    add_child(_profile_widget);

    _profile_selector = new ProfileSelector(cf::RectI(cf::PointI(window_size.width() - profile_wgt_size.width() - profile_offset,
                                                                 profile_offset),
                                                      selector_size));

    _profile_selector->profile_created.connect(this, &MainMenu::on_new_profile_created);
    _profile_selector->profile_create_begin.connect(this, &MainMenu::on_begin_profile_creation);
    _profile_selector->profile_create_end.connect(this, &MainMenu::on_end_profile_creation);
    _profile_selector->next_clicked.connect(this, &MainMenu::on_next_profile_clicked);
    _profile_selector->prev_clicked.connect(this, &MainMenu::on_prev_profile_clicked);

    add_child(_profile_selector);

    if (not _all_profiles.empty())
    {
        _active_profile = _all_profiles.front();
        _profile_widget->set_profile(_active_profile);
    }

    update_main_widgets();
}


int MainMenu::index_of_active_profile() const
{
    if (_active_profile == nullptr)
    {
        return -1;
    }

    auto iter = std::find(_all_profiles.begin(), _all_profiles.end(), _active_profile);

    assert(iter != _all_profiles.end());

    return iter - _all_profiles.begin();
}


bool MainMenu::is_new_profile_name_valid(const std::string &name) const
{
    return not name.empty()
       and std::none_of(_all_profiles.begin(),
                        _all_profiles.end(),
                        [&name](auto &&profile) { return profile->get_name() == name; });
}


bool MainMenu::has_previous_profile() const
{
    int index = index_of_active_profile();

    return index != -1 and index != 0;
}


bool MainMenu::has_next_profile() const
{
    int index = index_of_active_profile();

    return index != -1 and index < (_all_profiles.size() - 1);
}


bool MainMenu::can_start_with_active_profile() const
{
    return _active_profile != nullptr;
}


void MainMenu::update_main_widgets()
{
    _start_game_button->set_enabled(can_start_with_active_profile());

    _profile_selector->set_next_enabled(has_next_profile());
    _profile_selector->set_prev_enabled(has_previous_profile());
}


void MainMenu::on_next_profile_clicked()
{
    assert(has_next_profile());

    _active_profile = _all_profiles[index_of_active_profile() + 1];
    _profile_widget->set_profile(_active_profile);

    update_main_widgets();
}


void MainMenu::on_prev_profile_clicked()
{
    assert(has_previous_profile());

    _active_profile = _all_profiles[index_of_active_profile() - 1];
    _profile_widget->set_profile(_active_profile);

    update_main_widgets();
}


void MainMenu::on_begin_profile_creation()
{
    _start_game_button->set_enabled(false);
    _options_button->set_enabled(false);
}


void MainMenu::on_end_profile_creation()
{
    update_main_widgets();
    _options_button->set_enabled(true);
}


void MainMenu::on_new_profile_created(std::shared_ptr<Profile> new_profile)
{
    if (new_profile == nullptr)
    {
        return;
    }

    _all_profiles.emplace_back(new_profile);
    _active_profile = new_profile;
    _profile_widget->set_profile(new_profile);

    update_main_widgets();
}
