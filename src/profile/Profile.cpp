#include "Profile.hpp"

#include <filesystem>
#include <iostream>


#define PARAM_NAME(param) #param
#define PARAM_PAIR(param) PARAM_NAME(param), param
#define PARAM_DEF(param) static constexpr const char *param = PARAM_NAME(param)


std::shared_ptr<Profile> Profile::create_new(std::string name)
{
    std::shared_ptr<Profile> obj(new Profile(file_dir + name + file_ext));

    obj->name = std::move(name);
    obj->save();

    return obj;
}


std::shared_ptr<Profile> Profile::load_from_file(const std::string &file_path)
{
    if (not std::filesystem::exists(file_path))
    {
        std::cout << "Profile path '" << file_path << "' not found\n";
        return nullptr;
    }

    std::shared_ptr<Profile> obj(new Profile(file_path));

    if (obj->config.has_errors_on_load())
    {
        obj->config.set_need_auto_save(false);
        std::cout << "Profile file '" << file_path << "' corrupted\n";
        return nullptr;
    }

    try
    {
        obj->name = obj->config.get_value<std::string>(PARAM_NAME(name));
    }
    catch (const cf::Exception &ex)
    {
        std::cout << "Profile file '" << file_path << "' corrupted\n"
                  << ex.what() << std::endl;
        return nullptr;
    }

    obj->money = obj->config.get_value_opt<int>(PARAM_NAME(money)).value_or(0);

    return obj;
}


Profile::Profile(const std::string &file)
    : config(file)
{
}


Profile::~Profile()
{
    if (config.need_auto_save())
    {
        set_values_to_config();
    }
}


void Profile::save()
{
    set_values_to_config();
    config.save();
}


void Profile::set_values_to_config()
{
    config.clear();

    config.set_value(PARAM_PAIR(name));
    config.set_value(PARAM_PAIR(money));
}


#undef PARAM_PAIR
#undef PARAM_NAME
