#pragma once

#include "cf_ui/Widget.hpp"
#include "cf_util/Signal.hpp"


namespace cf
{
    class Button;
    class LineEdit;
} // namespace cf


class Profile;


/**
 * @brief Виджет выбора профиля
 */
class ProfileSelector : public cf::Widget
{
    cf::Button *next = nullptr;
    cf::Button *prev = nullptr;
    cf::Button *create = nullptr;
    cf::Button *ok = nullptr;
    cf::Button *cancel = nullptr;
    cf::LineEdit *edit = nullptr;

    cf::Widget *old_focus = nullptr;

public:
    /**
     * @brief Виджет выбора профиля
     *
     * @param rect Расположение и размер виджета
     */
    ProfileSelector(const cf::Rect2D &rect);

    /**
     * @brief Нажата кнопка "Следующий"
     */
    cf::Signal<> next_clicked;

    /**
     * @brief Нажата кнопка "Прерыдущий"
     */
    cf::Signal<> prev_clicked;

    /**
     * @brief Событие начала процесса создания нового профиля
     */
    cf::Signal<> profile_create_begin;

    /**
     * @brief Событие окончания создания нового профиля
     */
    cf::Signal<> profile_create_end;

    /**
     * @brief Создан новый профиль
     *
     * @note Будет отправлено после profile_create_end
     */
    cf::Signal<std::shared_ptr<Profile>> profile_created;

    /**
     * @brief Установить доступность кнопки "Следующий"
     */
    void set_next_enabled(bool enabled);

    /**
     * @brief Установить доступность кнопки "Предыдущий"
     */
    void set_prev_enabled(bool enabled);

private:
    void on_create_clicked();
    void on_ok_clicked();
    void on_cancel_clicked();
    void on_edit_text_changed(const std::string &text);
};
