#pragma once

#include <memory>
#include <string>

#include "cf_util/Configuration.hpp"


/**
 * @brief Профиль игрока
 *
 * Профиль игрока. Аккумулирует все базовые характеристики игрока и занимается выгрузкой их в файл.
 */
class Profile
{
    std::string name;
    std::string file_path;

    int money = 0;

    cf::Configuration config;

public:
    static constexpr const char *file_dir = "profiles/"; ///< Директория с провилями
    static constexpr const char *file_ext = ".prof";     ///< Расширение файла профиля

    ~Profile();

    Profile(Profile &) = delete;
    Profile(Profile &&) = delete;
    Profile &operator=(Profile &) = delete;
    Profile &operator=(Profile &&) = delete;

    /**
     * @brief Создать новый профиль
     *
     * @param name Имя профиля
     * @return Указатель на объект
     */
    static std::shared_ptr<Profile> create_new(std::string name);

    /**
     * @brief Загрузить профиль из файла
     *
     * @param file_path Путь к файлу
     * @return Загруженный профиль или nullptr в случае ошибки
     */
    static std::shared_ptr<Profile> load_from_file(const std::string &file_path);

    /**
     * @brief Получить имя профиля
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Установить имя профиля
     */
    inline void set_name(std::string new_name)
    {
        name = std::move(new_name);
    }

    /**
     * @brief Получить количество денег
     */
    inline int get_money() const
    {
        return money;
    }

    /**
     * @brief Установить количество денег
     */
    inline void set_money(int value)
    {
        money = value;
    }

private:
    Profile(const std::string &file);

    void save();
    void set_values_to_config();
};
