#include "ProfileSelector.hpp"

#include "cf_ui/Application.hpp"
#include "cf_ui/Button.hpp"
#include "cf_ui/LineEdit.hpp"
#include "cf_ui/Window.hpp"
#include "cf_ui/layout/Offset.hpp"
#include "cf_ui/layout/no_resize/Anchor.hpp"
#include "cf_ui/layout/no_resize/Horizontal.hpp"

#include "profile/Profile.hpp"


ProfileSelector::ProfileSelector(const cf::Rect2D &rect)
    : cf::Widget(rect)
{
    static const cf::Vector2D button_size = {75, 75};

    prev = new cf::Button(button_size);
    prev->set_text("<");
    prev->clicked.connect(prev_clicked);
    add_child(prev);

    next = new cf::Button(button_size);
    next->set_text(">");
    next->clicked.connect(next_clicked);
    add_child(next);

    create = new cf::Button(button_size);
    create->set_text("+");
    create->clicked.connect(this, &ProfileSelector::on_create_clicked);
    add_child(create);

    using namespace cf::layout;
    using namespace cf::layout::no_resize;

    constexpr double global_offset = 5;

    (Anchor(Anchor::Bottom,
            Offset{.value = global_offset, .type = Offset::Absolute})
     and Horizontal(Offset{.value = global_offset, .type = Offset::Absolute},
                    Offset{.value = global_offset, .type = Offset::Absolute}))
        .compose(this);

    ok = new cf::Button(prev->get_position(),
                        button_size);
    ok->set_text("Ok");
    ok->clicked.connect(this, &ProfileSelector::on_ok_clicked);
    ok->set_visible(false);
    add_child(ok);

    cancel = new cf::Button(next->get_position(),
                            cf::Vector2D(rect.get_size().dx - next->get_position().x - global_offset,
                                         button_size.dy));
    cancel->set_text("Отмена");
    cancel->clicked.connect(this, &ProfileSelector::on_cancel_clicked);
    cancel->set_visible(false);
    add_child(cancel);

    edit = new cf::LineEdit(cf::Point2D(global_offset,
                                        global_offset),
                            cf::Vector2D(rect.get_size().dx - global_offset * 2,
                                         button_size.dy));
    edit->set_visible(false);
    edit->set_chars_limit(10);
    edit->set_placeholder("Имя профиля");
    edit->text_changed.connect(this, &ProfileSelector::on_edit_text_changed);
    edit->set_validator([](char c) -> bool {
        return (c >= 'a' and c <= 'z')
            or (c >= 'A' and c <= 'Z')
            or (c >= '0' and c <= '9');
    });
    add_child(edit);
}


void ProfileSelector::set_next_enabled(bool enabled)
{
    next->set_enabled(enabled);
}


void ProfileSelector::set_prev_enabled(bool enabled)
{
    prev->set_enabled(enabled);
}


void ProfileSelector::on_create_clicked()
{
    ok->set_visible(true);
    ok->set_enabled(false);

    cancel->set_visible(true);

    edit->set_visible(true);
    edit->set_text("");

    next->set_visible(false);
    prev->set_visible(false);
    create->set_visible(false);

    auto &&wnd = cf::Application::get_instance().get_window();

    old_focus = wnd->get_keyboard_focus();
    wnd->set_keyboard_focus(edit);

    profile_create_begin.emit();
}


void ProfileSelector::on_ok_clicked()
{
    ok->set_visible(false);
    cancel->set_visible(false);
    edit->set_visible(false);

    next->set_visible(true);
    prev->set_visible(true);
    create->set_visible(true);

    auto &&wnd = cf::Application::get_instance().get_window();

    wnd->set_keyboard_focus(old_focus);

    profile_create_end.emit();
    profile_created.emit(Profile::create_new(edit->get_text()));
}


void ProfileSelector::on_cancel_clicked()
{
    ok->set_visible(false);
    cancel->set_visible(false);
    edit->set_visible(false);

    next->set_visible(true);
    prev->set_visible(true);
    create->set_visible(true);

    auto &&wnd = cf::Application::get_instance().get_window();

    wnd->set_keyboard_focus(old_focus);

    profile_create_end.emit();
}


void ProfileSelector::on_edit_text_changed(const std::string &text)
{
    ok->set_enabled(not text.empty());
}
