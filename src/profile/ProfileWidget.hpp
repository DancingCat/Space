#pragma once

#include <memory>

#include "cf_ui/Widget.hpp"


class Profile;

namespace cf
{
    class Label;
} // namespace cf


class ProfileWidget : public cf::Widget
{
    std::shared_ptr<Profile> profile;
    cf::Label *name_label = nullptr;
    cf::Label *money_label = nullptr;

public:
    ProfileWidget(const cf::Rect2D &rect, std::shared_ptr<Profile> user_profile);

    inline void set_profile(std::shared_ptr<Profile> new_profile)
    {
        profile = std::move(new_profile);
        update();
    }

    inline std::shared_ptr<Profile> get_profile() const
    {
        return profile;
    }

    void update();
};
