#include "ProfileWidget.hpp"

#include <cassert>

#include "cf_ui/Label.hpp"
#include "cf_ui/Style.hpp"

#include "Profile.hpp"


ProfileWidget::ProfileWidget(const cf::Rect2D &rect, std::shared_ptr<Profile> user_profile)
    : cf::Widget(rect)
    , profile(std::move(user_profile))
{
    constexpr double height = 50;
    constexpr double offset = 5;

    name_label = new cf::Label(cf::Point2D(0, offset),
                               cf::Vector2D(rect.get_size().dx, height));
    name_label->set_halign(cf::HAlign::Left);
    add_child(name_label);

    money_label = new cf::Label(cf::Point2D(0, height + offset * 2),
                                cf::Vector2D(rect.get_size().dx, height));
    money_label->set_halign(cf::HAlign::Left);
    add_child(money_label);

    cf::StyleEditor editor;
    editor.set_foreground_color(cf::Color{0xff, 0xff, 0xff});
    set_style(editor.get_result());

    update();
}


void ProfileWidget::update()
{
    if (profile == nullptr)
    {
        name_label->set_text("(Не выбран)");
        money_label->set_visible(false);
    }
    else
    {
        name_label->set_text(profile->get_name());

        money_label->set_visible(true);
        money_label->set_text("Ресурс: " + std::to_string(profile->get_money()));
    }
}
