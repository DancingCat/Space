#pragma once

#include "hangar/HangarWidget.hpp"


/**
 * @brief Виджет выбора улучшений
 */
class UpgradesWidget final : public HangarWidget::MainView
{
public:
    /**
     * @brief Виджет выбора улучшений
     *
     * @param rect Расположение и размер виджета
     */
    UpgradesWidget(const cf::Rect2D &rect);

protected:
    void paint_event() override;
};
