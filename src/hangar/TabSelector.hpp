#pragma once

#include "cf_ui/Widget.hpp"
#include "cf_util/Signal.hpp"


/**
 * @brief Виджет выбора вкладки ангара
 */
class TabSelector final : public cf::Widget
{
public:
    /**
     * @brief Виджет выбора вкладки ангара
     *
     * @param rect Расположение и размер виджета
     */
    TabSelector(const cf::Rect2D &rect);

    /**
     * @brief Выбрана карта
     */
    cf::Signal<> map_selected;

    /**
     * @brief Выбран магазин
     */
    cf::Signal<> shop_selected;

    /**
     * @brief Выбрано окно улучшений
     */
    cf::Signal<> upgrades_selected;
};
