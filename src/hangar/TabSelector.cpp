#include "TabSelector.hpp"

#include "cf_ui/Button.hpp"
#include "cf_ui/layout/Offset.hpp"
#include "cf_ui/layout/no_resize/Anchor.hpp"
#include "cf_ui/layout/no_resize/Horizontal.hpp"


TabSelector::TabSelector(const cf::Rect2D &rect)
    : cf::Widget(rect)
{
    static const cf::Vector2D button_size = {250, 75};

    auto map_button = new cf::Button(button_size);
    map_button->set_text("Карта");
    map_button->clicked.connect(map_selected);
    add_child(map_button);

    auto shop_button = new cf::Button(button_size);
    shop_button->set_text("Магазин");
    shop_button->clicked.connect(shop_selected);
    add_child(shop_button);

    auto upgrades_button = new cf::Button(button_size);
    upgrades_button->set_text("Улучшения");
    upgrades_button->clicked.connect(upgrades_selected);
    add_child(upgrades_button);

    using namespace cf::layout;
    using namespace cf::layout::no_resize;

    set_layout(Horizontal(Offset{.value = 0.05, .type = Offset::Relative},
                          Offset{.value = 0.05, .type = Offset::Relative})
               and Anchor(Anchor::Top,
                          Offset{.value = (rect.get_size().dy - button_size.dy) / 2, .type = Offset::Absolute}));
}
