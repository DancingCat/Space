#pragma once

#include <memory>
#include <vector>

#include "hangar/HangarWidget.hpp"


class Level;
class LevelButton;
class LevelPreviewWidget;


/**
 * @brief Виджет карты с выбором уровня
 */
class MapWidget final : public HangarWidget::MainView
{
    std::vector<LevelButton *> level_buttons;
    LevelButton *current = nullptr;
    LevelPreviewWidget *preview = nullptr;
    bool mouse_pressed = false;
    bool moved = false;

public:
    /**
     * @brief Виджет карты с выбором уровня
     *
     * @param rect Расположение и размер виджета
     */
    MapWidget(const cf::Rect2D &rect);

    /**
     * @brief Нажата кнопка "Начать"
     */
    cf::Signal<std::shared_ptr<Level>> start_clicked;

protected:
    void paint_event() override;
    void mouse_out_event() override;
    void mouse_button_event(cf::MouseButtonEvent *event) override;
    void mouse_move_event(cf::MouseMoveEvent *event) override;
};
