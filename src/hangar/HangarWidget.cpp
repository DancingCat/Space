#include "HangarWidget.hpp"

#include "cf_ui/Button.hpp"

#include "game/Spaceship.hpp"
#include "hangar/MapWidget.hpp"
#include "hangar/ShopWidget.hpp"
#include "hangar/TabSelector.hpp"
#include "hangar/UpgradesWidget.hpp"

#include "game/SpaceshipBuilder.hpp"


HangarWidget::HangarWidget(cf::Vector2D window_size)
    : cf::Widget(cf::Rect2D(cf::Point2D::null(), window_size))
{
    constexpr double top_bar_height = 100;
    constexpr double exit_to_main_width = 1000;
    constexpr cf::Vector2D exit_to_main_size = {351, 75};
    constexpr double exit_to_main_offset = top_bar_height / 2 - exit_to_main_size.dy / 2;

    cf::Rect2D main_rect(cf::Point2D(0,
                                     top_bar_height),
                         cf::Vector2D(window_size.dx,
                                      window_size.dy - top_bar_height));

    auto map_widget = new MapWidget(main_rect);
    map_widget->start_clicked.connect([this](std::shared_ptr<Level> level) {
        // todo
        SpaceshipBuilder builder;
        start_clicked.emit(builder.get_result(), std::move(level));
    });
    add_child(map_widget);
    set_active_widget(map_widget);

    auto upgrades_widget = new UpgradesWidget(main_rect);
    add_child(upgrades_widget)->set_visible(false);

    auto shop_widget = new ShopWidget(main_rect);
    add_child(shop_widget)->set_visible(false);

    auto tab_selector = new TabSelector(cf::Rect2D(cf::Point2D::null(),
                                                   cf::Vector2D(window_size.dx - exit_to_main_width,
                                                                top_bar_height)));
    add_child(tab_selector);

    tab_selector->map_selected.connect([this, map_widget]() { set_active_widget(map_widget); });
    tab_selector->shop_selected.connect([this, shop_widget]() { set_active_widget(shop_widget); });
    tab_selector->upgrades_selected.connect([this, upgrades_widget]() { set_active_widget(upgrades_widget); });

    auto exit_button = new cf::Button(cf::Point2D(window_size.dx - exit_to_main_size.dx - exit_to_main_offset,
                                                  exit_to_main_offset),
                                      exit_to_main_size);
    exit_button->set_text("Выйти в главное меню");
    exit_button->clicked.connect(exit_to_main);
    add_child(exit_button);
}


void HangarWidget::set_active_widget(MainView *widget)
{
    if (active_widget != nullptr)
    {
        active_widget->set_visible(false);
        active_widget->on_deactivated();
    }

    active_widget = widget;

    if (active_widget != nullptr)
    {
        active_widget->on_activated();
        active_widget->set_visible(true);
    }
}
