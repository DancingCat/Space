#include "MapWidget.hpp"

#include <array>
#include <cassert>

#include "cf_ui/Button.hpp"
#include "cf_ui/Painter.hpp"
#include "cf_ui/event/MouseButtonEvent.hpp"
#include "cf_ui/event/MouseMoveEvent.hpp"

#include "game/Level.hpp"
#include "game/LevelPreviewWidget.hpp"


/**
 * @brief Кнопка игрового уровня на карте
 */
class LevelButton : public cf::Button
{
    static constexpr cf::Vector2D button_size = {100, 100};

    std::shared_ptr<Level> level;
    bool open = false;

public:
    /**
     * @brief Кнопка игрового уровня на карте
     *
     * @param pos Относительная позиция на карте
     * @param level Соответствующий кнопке уровень
     */
    LevelButton(cf::Point2D pos, std::shared_ptr<Level> level)
        : cf::Button(pos, button_size)
        , level(std::move(level))
    {
        assert(this->level != nullptr);

        set_text(this->level->get_short_name());
        set_enabled(false);
    }

    /**
     * @brief Получить соответствующий кнопке уровень
     */
    std::shared_ptr<Level> get_level()
    {
        return level;
    }

    /**
     * @brief Доступен ли уровень для выбора
     */
    bool is_open() const
    {
        return open;
    }

    /**
     * @brief Установить доступность выбора уровня
     */
    void set_open(bool value)
    {
        open = value;
        set_enabled(open);
    }
};


MapWidget::MapWidget(const cf::Rect2D &rect)
    : HangarWidget::MainView(rect)
{
    struct LevelInfo
    {
        cf::Point2D pos;
        const char *short_name;
        const char *name;
        const char *desc;
    };

    constexpr std::array<LevelInfo, 5> level_data = {
        LevelInfo{.pos = cf::Point2D{0.1, 0.8},  .short_name = "T.1", .name = "Тестовый уровень 1", .desc = "Тестовый уровень\nБез описания"},
        LevelInfo{.pos = cf::Point2D{0.2, 0.4},  .short_name = "T.2", .name = "Тестовый уровень 2", .desc = "Тестовый уровень"                         },
        LevelInfo{.pos = cf::Point2D{0.5, 0.7},  .short_name = "T.3", .name = "Тестовый уровень 3", .desc = "Тестовый уровень"                         },
        LevelInfo{.pos = cf::Point2D{0.8, 0.8},  .short_name = "T.4", .name = "Тестовый уровень 4", .desc = "Тестовый уровень"                         },
        LevelInfo{.pos = cf::Point2D{0.75, 0.1}, .short_name = "T.5", .name = "Тестовый уровень 5", .desc = "Тестовый уровень"                         }
    };

    for (auto &&[pos, short_name, name, desc] : level_data)
    {
        auto level = std::make_shared<Level>();
        level->set_short_name(short_name);
        level->set_name(name);
        level->set_description(desc);

        auto button = new LevelButton(cf::Point2D(pos.x * rect.get_size().dx,
                                                  pos.y * rect.get_size().dy),
                                      std::move(level));
        button->clicked.connect([button, this] {
            current = button;
            preview->set_active_level(current->get_level());
            preview->set_position(cf::Point2D(button->get_position().x + button->get_size().dx + 15,
                                              button->get_position().y));
        });
        level_buttons.push_back(button);
        add_child(button);
    }

    level_buttons[0]->set_open(true);
    level_buttons[1]->set_open(true);

    constexpr double preview_width = 500;
    constexpr double preview_offset = 5;

    preview = new LevelPreviewWidget(cf::Rect2D::null(), nullptr);
    preview->start_clicked.connect(start_clicked);
    add_child(preview);
}


void MapWidget::paint_event()
{
    if (level_buttons.empty())
    {
        HangarWidget::MainView::paint_event();
        return;
    }

    cf::Painter painter(this);

    auto iter = level_buttons.begin();
    cf::Button *ib = *iter;
    ++iter;

    for (; iter != level_buttons.end(); ++iter)
    {
        if ((*iter)->is_open())
        {
            painter.set_color({0x00, 0xff, 0x00});
        }
        else
        {
            painter.set_color({0xff, 0x00, 0x00});
        }

        painter.draw_line(cf::Line2D(ib->get_position() + ib->get_size() / 2,
                                     (*iter)->get_position() + (*iter)->get_size() / 2));
        ib = *iter;
    }

    if (current != nullptr)
    {
        constexpr cf::Vector2D offset = {10, 10};

        painter.set_color({0x80, 0x80, 0x80, 0x80});
        painter.draw_rect(cf::Rect2D(cf::Point2D(current->get_position() - offset),
                                     cf::Vector2D(current->get_size() + offset * 2)));
    }

    HangarWidget::MainView::paint_event();
}


void MapWidget::mouse_out_event()
{
    mouse_pressed = false;
}


void MapWidget::mouse_button_event(cf::MouseButtonEvent *event)
{
    if (event->get_button() == cf::MouseButton::Left)
    {
        mouse_pressed = event->is_pressed();

        if (not event->is_pressed() and not moved)
        {
            current = nullptr;
            preview->set_active_level(nullptr);
        }

        moved = false;
    }

    HangarWidget::MainView::mouse_button_event(event);
}


void MapWidget::mouse_move_event(cf::MouseMoveEvent *event)
{
    if (mouse_pressed)
    {
        for (auto &&item : level_buttons)
        {
            item->set_position(item->get_position() + event->get_relative_position());
        }

        preview->set_position(preview->get_position() + event->get_relative_position());

        moved = true;
    }

    HangarWidget::MainView::mouse_move_event(event);
}
