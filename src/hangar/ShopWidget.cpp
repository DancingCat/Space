#include "ShopWidget.hpp"

#include "cf_ui/Painter.hpp"


ShopWidget::ShopWidget(const cf::Rect2D &rect)
    : HangarWidget::MainView(rect)
{
}


void ShopWidget::paint_event()
{
    cf::Painter painter(this);
    painter.set_color({0x00, 0x00, 0x55});
    painter.fill_rect(cf::Rect2D(cf::Point2D::null() + cf::Vector2D{5, 5},
                                 get_size() - cf::Vector2D{5, 5} * 2));
}
