#pragma once

#include "hangar/HangarWidget.hpp"


/**
 * @brief Виджет магазина
 */
class ShopWidget : public HangarWidget::MainView
{
public:
    /**
     * @brief Виджет магазина
     *
     * @param rect Расположение и размер виджета
     */
    ShopWidget(const cf::Rect2D &rect);

protected:
    void paint_event() override;
};
