#pragma once

#include "cf_ui/Widget.hpp"
#include "cf_util/Signal.hpp"


class Spaceship;
class Level;


/**
 * @brief Виджет ангара
 */
class HangarWidget final : public cf::Widget
{
public:
    class MainView : public cf::Widget
    {
    public:
        inline MainView(const cf::Rect2D &rect)
            : cf::Widget(rect)
        {
        }

        inline virtual void on_activated()
        {
        }

        inline virtual void on_deactivated()
        {
        }
    };

private:
    MainView *active_widget = nullptr;

public:
    /**
     * @brief Виджет ангара
     *
     * @param window_size Размер окна (виджет подразумевается во все окно)
     */
    HangarWidget(cf::Vector2D window_size);

    /**
     * @brief Нажата кнопка "Начать" на карте
     */
    cf::Signal<std::shared_ptr<Spaceship>, std::shared_ptr<Level>> start_clicked;

    /**
     * @brief Нажата кнопка "Выйти в главное меню"
     */
    cf::Signal<> exit_to_main;

private:
    void set_active_widget(MainView *widget);
};
