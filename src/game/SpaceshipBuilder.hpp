#pragma once

#include <memory>


class Arm;
class Cockpit;
class Jet;
class Reactor;
class Weapon;
class Wing;
class Spaceship;


/**
 * @brief Сборщик кораблей
 */
class SpaceshipBuilder
{
    std::shared_ptr<Arm> _arm;
    std::shared_ptr<Cockpit> _cockpit;
    std::shared_ptr<Jet> _jet;
    std::shared_ptr<Reactor> _reactor;
    std::shared_ptr<Weapon> _weapon;
    std::shared_ptr<Wing> _wing;

public:
    /**
     * @brief Сборщик кораблей
     */
    SpaceshipBuilder();

    /**
     * @brief Сбросить параметры на умолчательные
     */
    void reset();

    /**
     * @brief Получить результат (nullptr если can_create() == false)
     */
    std::shared_ptr<Spaceship> get_result();

    /**
     * @brief Установить плечо
     */
    void set_arm(std::shared_ptr<Arm> obj);

    /**
     * @brief Установить кокпит
     */
    void set_cockpit(std::shared_ptr<Cockpit> obj);

    /**
     * @brief Установить двигатель
     */
    void set_jet(std::shared_ptr<Jet> obj);

    /**
     * @brief Установить реактор
     */
    void set_reactor(std::shared_ptr<Reactor> obj);

    /**
     * @brief Установить крылья
     */
    void set_wing(std::shared_ptr<Wing> obj);

    /**
     * @brief Установить оружие
     */
    void set_weapon(std::shared_ptr<Weapon> obj);

    /**
     * @brief Доступна ли сборка
     */
    bool can_create() const;
};
