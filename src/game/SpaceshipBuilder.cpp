#include "SpaceshipBuilder.hpp"

#include "cf_ui/Texture.hpp"

#include "game/Spaceship.hpp"
#include "game/spaceship/parts/arm/Arm.hpp"
#include "game/spaceship/parts/cockpit/Cockpit.hpp"
#include "game/spaceship/parts/jet/Jet.hpp"
#include "game/spaceship/parts/reactor/Reactor.hpp"
#include "game/spaceship/parts/weapon/Weapon.hpp"
#include "game/spaceship/parts/wing/Wing.hpp"


SpaceshipBuilder::SpaceshipBuilder()
{
    reset();
}


void SpaceshipBuilder::reset()
{
    _arm = Arm::create_default();
    _cockpit = Cockpit::create_default();
    _jet = Jet::create_default();
    _reactor = Reactor::create_default();
    _weapon = Weapon::create_default();
    _wing = Wing::create_default();
}


std::shared_ptr<Spaceship> SpaceshipBuilder::get_result()
{
    if (not can_create())
    {
        return nullptr;
    }

    PartStats stats = PartStats::merge(_cockpit->get_stats(),
                                       _jet->get_stats(),
                                       _reactor->get_stats(),
                                       _wing->get_stats());

    if (_arm != nullptr)
    {
        stats = PartStats::merge(stats, _arm->get_stats());
    }

    WeaponStats weapon_stats = _weapon->get_stats();
    weapon_stats.set_damage(weapon_stats.get_damage() * stats.relative.damage);
    weapon_stats.set_fire_rate(weapon_stats.get_fire_rate() * stats.relative.fire_rate);

    std::shared_ptr<Spaceship> obj{new Spaceship(stats.to_absolute_value())};
    obj->_texture_pack.emplace_back(_wing->get_texture());
    obj->_texture_pack.emplace_back(_jet->get_texture());
    obj->_texture_pack.emplace_back(_cockpit->get_texture());

    if (_arm != nullptr)
    {
        obj->_texture_pack.emplace_back(_arm->get_texture());
    }

    obj->_weapon_stats = weapon_stats;
    obj->_weapon_texture = _weapon->get_texture();

    return obj;
}


void SpaceshipBuilder::set_arm(std::shared_ptr<Arm> obj)
{
    _arm = std::move(obj);
}


void SpaceshipBuilder::set_cockpit(std::shared_ptr<Cockpit> obj)
{
    _cockpit = std::move(obj);
}


void SpaceshipBuilder::set_jet(std::shared_ptr<Jet> obj)
{
    _jet = std::move(obj);
}


void SpaceshipBuilder::set_reactor(std::shared_ptr<Reactor> obj)
{
    _reactor = std::move(obj);
}


void SpaceshipBuilder::set_wing(std::shared_ptr<Wing> obj)
{
    _wing = std::move(obj);
}


void SpaceshipBuilder::set_weapon(std::shared_ptr<Weapon> obj)
{
    _weapon = std::move(obj);
}


bool SpaceshipBuilder::can_create() const
{
    return _cockpit != nullptr
       and _jet != nullptr
       and _reactor != nullptr
       and _weapon != nullptr
       and _wing != nullptr;
}
