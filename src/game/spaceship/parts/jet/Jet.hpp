#pragma once

#include <memory>
#include <string>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/PartStats.hpp"


/**
 * @brief Дввигатель корабля
 *
 * Двигатель - обязательная часть корабля. Отвечает за распределение ускорения, максимальной скорости, защиты, прочности и щита.
 */
class Jet
{
    std::string name;

public:
    /**
     * @brief Создать умолчательный объект
     *
     * Умолчательный объект - объект, который установлен на немодифицированном корабле
     */
    static inline std::shared_ptr<Jet> create_default()
    {
        return create_basic();
    }

    /**
     * @brief Реактор
     *
     * @param name Название
     */
    inline Jet(std::string name)
        : name(std::move(name))
    {
    }

    virtual ~Jet() = default;

    Jet(Jet &) = delete;
    Jet(Jet &&) = delete;
    Jet &operator=(Jet &) = delete;
    Jet &operator=(Jet &&) = delete;

    /**
     * @brief Получить название
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Получить базовые характеристики
     */
    virtual const PartStats &get_basic_stats() const = 0;

    /**
     * @brief Получить характеристики
     */
    virtual const PartStats &get_stats() const = 0;

    /**
     * @brief Получить текстуру
     */
    virtual cf::Texture get_texture() const = 0;

    /**
     * @brief Создать базовый двигатель
     *
     * Базовый двигатель не меняет характеристики корабля.
     */
    static std::shared_ptr<Jet> create_basic();

    /**
     * @brief Создать скоростной двигатель
     *
     * Скоростной двигатель сильно увеличивает ускорение и максимальную скорость ценой защитных показателей.
     */
    static std::shared_ptr<Jet> create_accelerated();

    /**
     * @brief Сдвоенный двигатель
     *
     * Сдвоенный двигатель немного увеличивает ускорение, максимальную скорость и мощность щита без снижения других показателей.
     */
    static std::shared_ptr<Jet> create_doubled();

    /**
     * @brief Создать тяжелый двигатель
     *
     * Тяжелый двигатель значительно уввеличивает прочность, защиту и прочность щита корабля ценой максимальной скорости и ускорения.
     */
    static std::shared_ptr<Jet> create_heavy();
};
