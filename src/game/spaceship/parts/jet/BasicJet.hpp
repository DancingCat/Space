#pragma once

#include "game/spaceship/parts/jet/Jet.hpp"


/**
 * @brief Базовый двигатель
 *
 * Базовый двигатель не меняет характеристики корабля.
 */
class BasicJet : public Jet
{
    static constexpr auto name = "Базовый двигатель";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .shield = 0,
                     .max_speed = 0,
                     .thrust = 0   },
        .relative = {.shield = 1,
                     .max_speed = 1,
                     .damage = 1,
                     .fire_rate = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline BasicJet()
        : Jet(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/jet_basic.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
