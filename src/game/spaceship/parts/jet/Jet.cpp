#include "Jet.hpp"

#include "game/spaceship/parts/jet/AcceleratedJet.hpp"
#include "game/spaceship/parts/jet/BasicJet.hpp"
#include "game/spaceship/parts/jet/DoubledJet.hpp"
#include "game/spaceship/parts/jet/HeavyJet.hpp"


std::shared_ptr<Jet> Jet::create_basic()
{
    return std::make_shared<BasicJet>();
}


std::shared_ptr<Jet> Jet::create_accelerated()
{
    return std::make_shared<AcceleratedJet>();
}


std::shared_ptr<Jet> Jet::create_doubled()
{
    return std::make_shared<DoubledJet>();
}


std::shared_ptr<Jet> Jet::create_heavy()
{
    return std::make_shared<HeavyJet>();
}
