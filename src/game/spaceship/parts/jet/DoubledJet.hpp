#pragma once

#include "game/spaceship/parts/jet/Jet.hpp"


/**
 * @brief Сдвоенный двигатель
 *
 * Сдвоенный двигатель немного увеличивает ускорение, максимальную скорость и мощность щита без снижения других показателей.
 */
class DoubledJet : public Jet
{
    static constexpr auto name = "Сдвоенный двигатель";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .shield = 0,
                     .max_speed = 0,
                     .thrust = 0   },
        .relative = {.shield = 1,
                     .max_speed = 1,
                     .damage = 1,
                     .fire_rate = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline DoubledJet()
        : Jet(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/jet_doubled.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
