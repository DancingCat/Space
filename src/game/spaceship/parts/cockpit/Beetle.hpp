#pragma once

#include "game/spaceship/parts/cockpit/Cockpit.hpp"


/**
 * @brief Жучок
 *
 * Жучок - обеспечивает высокую скорость и управляемость, не очень прочный
 */
class Beetle : public Cockpit
{
    static constexpr auto name = "Жучок";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 1,
                     .health = 1,
                     .shield = 1,
                     .max_speed = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline Beetle()
        : Cockpit(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/cockpit_beetle.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
