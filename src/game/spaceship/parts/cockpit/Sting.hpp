#pragma once

#include "game/spaceship/parts/cockpit/Cockpit.hpp"


/**
 * @brief Жало
 *
 * Жало - основа со сниженной прочностью, но хорошей скоростью, маневренностью, щитом и высокой защитой.
 */
class Sting : public Cockpit
{
    static constexpr auto name = "Жало";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 1,
                     .health = 1,
                     .shield = 1,
                     .max_speed = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline Sting()
        : Cockpit(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/cockpit_sting.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
