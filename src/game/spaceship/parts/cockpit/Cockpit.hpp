#pragma once

#include <memory>
#include <string>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/PartStats.hpp"


/**
 * @brief Кокпит
 *
 * Кокпит - базовая часть корабля, определяет все характеристики кроме оружия.
 */
class Cockpit
{
    std::string name;

public:
    /**
     * @brief Создать умолчательный объект
     *
     * Умолчательный объект - выдается по-умолчанию
     */
    static inline std::shared_ptr<Cockpit> create_default()
    {
        return create_duckling();
    }

    /**
     * @brief Кокпит
     *
     * @param name Название
     */
    inline Cockpit(std::string name)
        : name(std::move(name))
    {
    }

    virtual ~Cockpit() = default;

    Cockpit(Cockpit &) = delete;
    Cockpit(Cockpit &&) = delete;
    Cockpit &operator=(Cockpit &) = delete;
    Cockpit &operator=(Cockpit &&) = delete;

    /**
     * @brief Получить название
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Получить базовые характеристики
     */
    virtual const PartStats &get_basic_stats() const = 0;

    /**
     * @brief Получить характеристики
     */
    virtual const PartStats &get_stats() const = 0;

    /**
     * @brief Получить текстуру
     */
    virtual cf::Texture get_texture() const = 0;

    /**
     * @brief Создать Утенка
     *
     * Утенок - стандартная основа корабля, имеет сбалансированные характеристики
     */
    static std::shared_ptr<Cockpit> create_duckling();

    /**
     * @brief Создать Жучка
     *
     * Жучок - обеспечивает высокую скорость и управляемость, не очень прочный
     */
    static std::shared_ptr<Cockpit> create_beetle();

    /**
     * @brief Создать большого босса
     *
     * Большой босс - массивный тяжелый корабль, очень высокие показатели защиты, прочности и щита компенсируются
     * крайне плохой управляемостью.
     */
    static std::shared_ptr<Cockpit> create_big_boss();

    /**
     * @brief Созжать Жало
     *
     * Жало - основа со сниженной прочностью, но хорошей скоростью, маневренностью, щитом и высокой защитой.
     */
    static std::shared_ptr<Cockpit> create_sting();
};
