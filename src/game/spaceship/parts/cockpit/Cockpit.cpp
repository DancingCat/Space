#include "Cockpit.hpp"

#include "game/spaceship/parts/cockpit/Beetle.hpp"
#include "game/spaceship/parts/cockpit/BigBoss.hpp"
#include "game/spaceship/parts/cockpit/Duckling.hpp"
#include "game/spaceship/parts/cockpit/Sting.hpp"


std::shared_ptr<Cockpit> Cockpit::create_duckling()
{
    return std::make_shared<Duckling>();
}


std::shared_ptr<Cockpit> Cockpit::create_beetle()
{
    return std::make_shared<Beetle>();
}


std::shared_ptr<Cockpit> Cockpit::create_big_boss()
{
    return std::make_shared<BigBoss>();
}


std::shared_ptr<Cockpit> Cockpit::create_sting()
{
    return std::make_shared<Sting>();
}
