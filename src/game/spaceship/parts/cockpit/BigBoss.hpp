#pragma once

#include "game/spaceship/parts/cockpit/Cockpit.hpp"


/**
 * @brief Большой босс
 *
 * Большой босс - массивный тяжелый корабль, очень высокие показатели защиты, прочности и щита компенсируются
 * крайне плохой управляемостью.
 */
class BigBoss : public Cockpit
{
    static constexpr auto name = "Большой босс";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 1,
                     .health = 1,
                     .shield = 1,
                     .max_speed = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline BigBoss()
        : Cockpit(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/cockpit_big_boss.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
