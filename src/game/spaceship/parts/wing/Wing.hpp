#pragma once

#include <memory>
#include <string>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/PartStats.hpp"


/**
 * @brief Крыло
 *
 * Крыло меняет подвижность корабля (в большей степени ускорение) и защитные показатели (кроме щита)
 */
class Wing
{
    const std::string name;

public:
    /**
     * @brief Создать умолчательный объект
     *
     * Умолчательный объект - объект, который установлен на немодифицированном корабле
     */
    static inline std::shared_ptr<Wing> create_default()
    {
        return create_basic();
    }

    /**
     * @brief Создать базовое крыло
     *
     * Базовое крыло не дает бонусов к характеристикам. Является необходимой частью корабля.
     */
    static std::shared_ptr<Wing> create_basic();

    /**
     * @brief Создать облегченное крыло
     *
     * Облегченное крыло значительно повышает маневренность (ускорение) и незначительно максимальную скорость,
     * незначительно снижается защита корабля.
     */
    static std::shared_ptr<Wing> create_lightweight();

    /**
     * @brief Создать штурмовое крыло
     *
     * Штурмовое крыло значительно повышает максимальную скорость и ускорение ценой защиты и прочности.
     */
    static std::shared_ptr<Wing> create_assault();

    /**
     * @brief Создать тяжелое крыло
     *
     * Тяжелое крыло значительно повышает защиту и прочность ценой максимальной скорости и ускорения.
     */
    static std::shared_ptr<Wing> create_heavy();

    /**
     * @brief Крыло
     *
     * @param name Название
     */
    inline Wing(std::string name)
        : name(std::move(name))
    {
    }

    virtual ~Wing() = default;

    Wing(Wing &) = delete;
    Wing(Wing &&) = delete;
    Wing &operator=(Wing &) = delete;
    Wing &operator=(Wing &&) = delete;

    /**
     * @brief Получить название
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Получить базовые характеристики
     */
    virtual const PartStats &get_basic_stats() const = 0;

    /**
     * @brief Получить характеристики
     */
    virtual const PartStats &get_stats() const = 0;

    /**
     * @brief Получить текстуру
     */
    virtual cf::Texture get_texture() const = 0;
};
