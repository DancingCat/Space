#include "Wing.hpp"

#include "game/spaceship/parts/wing/AssaultWing.hpp"
#include "game/spaceship/parts/wing/BasicWing.hpp"
#include "game/spaceship/parts/wing/HeavyWing.hpp"
#include "game/spaceship/parts/wing/LightweightWing.hpp"


std::shared_ptr<Wing> Wing::create_basic()
{
    return std::make_shared<BasicWing>();
}


std::shared_ptr<Wing> Wing::create_lightweight()
{
    return std::make_shared<LightweightWing>();
}


std::shared_ptr<Wing> Wing::create_assault()
{
    return std::make_shared<AssaultWing>();
}


std::shared_ptr<Wing> Wing::create_heavy()
{
    return std::make_shared<HeavyWing>();
}
