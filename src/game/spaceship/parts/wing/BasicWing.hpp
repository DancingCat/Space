#pragma once

#include "Wing.hpp"

/**
 * @brief Базовое крыло
 *
 * Базовое крыло не дает бонусов к характеристикам. Является необходимой частью корабля.
 */
class BasicWing : public Wing
{
    static constexpr auto name = "Базовое крыло";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .health = 0,
                     .armor = 0,
                     .max_speed = 0,
                     .thrust = 0},
        .relative = {.health = 1,
                     .armor = 1,
                     .max_speed = 1,
                     .thrust = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline BasicWing()
        : Wing(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/wing_basic.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
