#pragma once

#include <memory>
#include <string>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/PartStats.hpp"


/**
 * @brief Плечо
 *
 * Плечо является опциональной частью корабля. Изменяет защиту, прочность и скорость.
 */
class Arm
{
    std::string name;

public:
    /**
     * @brief Создать умолчательный объект
     *
     * Умолчательный объект - объект, который установлен на немодифицированном корабле
     */
    static inline std::shared_ptr<Arm> create_default()
    {
        return nullptr;
    }

    /**
     * @brief Плечо
     *
     * @param name Название
     */
    inline Arm(std::string name)
        : name(std::move(name))
    {
    }

    virtual ~Arm() = default;

    Arm(Arm &) = delete;
    Arm(Arm &&) = delete;
    Arm &operator=(Arm &) = delete;
    Arm &operator=(Arm &&) = delete;

    /**
     * @brief Получить название
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Получить базовые характеристики
     */
    virtual const PartStats &get_basic_stats() const = 0;

    /**
     * @brief Получить характеристики
     */
    virtual const PartStats &get_stats() const = 0;

    /**
     * @brief Получить текстуру
     */
    virtual cf::Texture get_texture() const = 0;

    /**
     * @brief Базовое плечо
     *
     * Базовое плечо дает равный бонус к прочности, скорости и защите.
     */
    static std::shared_ptr<Arm> create_basic();

    /**
     * @brief Двойное базовое плечо
     *
     * Двойное базовое плечо дает удвоенный бонус к прочности и защите, но не дает бонус к скорости.
     */
    static std::shared_ptr<Arm> create_double_basic();

    /**
     * @brief Штурмовое плечо
     *
     * Штурмовое плечо дает высокий бонус к скорости, но снижает прочность и защиту.
     */
    static std::shared_ptr<Arm> create_assault();

    /**
     * @brief Тяжелое
     *
     * Тяжелое плечо дает высокий бонус к прочности и защите, но снижает скорость.
     */
    static std::shared_ptr<Arm> create_heavy();
};
