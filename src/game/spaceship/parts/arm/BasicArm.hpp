#pragma once

#include "game/spaceship/parts/arm/Arm.hpp"


/**
 * @brief Базовое плечо
 *
 * Базовое плечо дает равный бонус к прочности, скорости и защите.
 */
class BasicArm : public Arm
{
    static constexpr auto name = "Базовое плечо";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .health = 0,
                     .shield = 0,
                     .armor = 0,
                     .max_speed = 0},
        .relative = {.health = 1,
                     .armor = 1,
                     .max_speed = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline BasicArm()
        : Arm(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/arm_basic.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
