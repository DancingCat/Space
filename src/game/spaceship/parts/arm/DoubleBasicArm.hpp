#pragma once

#include "game/spaceship/parts/arm/Arm.hpp"


/**
 * @brief Двойное базовое плечо
 *
 * Двойное базовое плечо дает удвоенный бонус к прочности и защите, но не дает бонус к скорости.
 */
class DoubleBasicArm : public Arm
{
    static constexpr auto name = "Двойное базовое плечо";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .health = 0,
                     .shield = 0,
                     .armor = 0,
                     .max_speed = 0},
        .relative = {.health = 1,
                     .armor = 1,
                     .max_speed = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline DoubleBasicArm()
        : Arm(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/arm_double_basic.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
