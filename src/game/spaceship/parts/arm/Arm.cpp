#include "Arm.hpp"

#include "game/spaceship/parts/arm/AssaultArm.hpp"
#include "game/spaceship/parts/arm/BasicArm.hpp"
#include "game/spaceship/parts/arm/DoubleBasicArm.hpp"
#include "game/spaceship/parts/arm/HeavyArm.hpp"


std::shared_ptr<Arm> Arm::create_basic()
{
    return std::make_shared<BasicArm>();
}


std::shared_ptr<Arm> Arm::create_double_basic()
{
    return std::make_shared<DoubleBasicArm>();
}


std::shared_ptr<Arm> Arm::create_assault()
{
    return std::make_shared<AssaultArm>();
}


std::shared_ptr<Arm> Arm::create_heavy()
{
    return std::make_shared<HeavyArm>();
}
