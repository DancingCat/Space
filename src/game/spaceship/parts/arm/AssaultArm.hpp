#pragma once

#include "game/spaceship/parts/arm/Arm.hpp"


/**
 * @brief Штурмовое плечо
 *
 * Штурмовое плечо дает высокий бонус к скорости, но снижает прочность и защиту.
 */
class AssaultArm : public Arm
{
    static constexpr auto name = "Штурмовое плечо";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .health = 0,
                     .shield = 0,
                     .armor = 0,
                     .max_speed = 0},
        .relative = {.health = 1,
                     .armor = 1,
                     .max_speed = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline AssaultArm()
        : Arm(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/arm_assault.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
