#pragma once

#include <functional>

#include "game/spaceship/parts/weapon/Weapon.hpp"


/**
 * @brief Миниган
 *
 * Миниган - скорострельное оружие с невысоким уроном не сниженной точностью
 */
class Minigun : public Weapon
{
    static inline WeaponStats basic_stats = {};

    cf::Texture texture;
    WeaponStats stats;

public:
    Minigun()
        : Weapon("Миниган")
        , texture(cf::Texture::load_from_file("assets/player/parts/weapon_minigun.png"))
    {
        [[maybe_unused]] static bool init_basic_stats_flag = std::invoke([] {
            basic_stats.set_automatic(true);
            basic_stats.set_accuracy(0.65);
            basic_stats.set_damage(8);
            basic_stats.set_damage_delta(0.45);
            basic_stats.set_fire_rate(15);
            return true;
        });

        stats = basic_stats;
    }

    const WeaponStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    const WeaponStats &get_stats() const override
    {
        return stats;
    }

    cf::Texture get_texture() const override
    {
        return texture;
    }
};
