#pragma once

#include <functional>

#include "game/spaceship/parts/weapon/Weapon.hpp"


/**
 * @brief Бластер
 *
 * Бластер - автоматическое оружие со сбалансированной точностью и уроном
 */
class Blaster : public Weapon
{
    static inline WeaponStats basic_stats = {};

    cf::Texture texture;
    WeaponStats stats;

public:
    Blaster()
        : Weapon("Бластер")
        , texture(cf::Texture::load_from_file("assets/player/parts/weapon_blaster.png"))
    {
        [[maybe_unused]] static bool init_basic_stats_flag = std::invoke([] {
            basic_stats.set_automatic(true);
            basic_stats.set_accuracy(0.8);
            basic_stats.set_damage(20);
            basic_stats.set_damage_delta(0.2);
            basic_stats.set_fire_rate(5);
            return true;
        });

        stats = basic_stats;
    }

    const WeaponStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    const WeaponStats &get_stats() const override
    {
        return stats;
    }

    cf::Texture get_texture() const override
    {
        return texture;
    }
};
