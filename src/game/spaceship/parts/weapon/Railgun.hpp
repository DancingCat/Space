#pragma once

#include <functional>

#include "game/spaceship/parts/weapon/Weapon.hpp"


/**
 * @brief Реальса
 *
 * Реальса - полу-автоматическое оружие с низкой скорострельностью, но высокими уроном и точностью
 */
class Railgun : public Weapon
{
    static inline WeaponStats basic_stats = {};

    cf::Texture texture;
    WeaponStats stats;

public:
    Railgun()
        : Weapon("Рельса")
        , texture(cf::Texture::load_from_file("assets/player/parts/weapon_railgun.png"))
    {
        [[maybe_unused]] static bool init_basic_stats_flag = std::invoke([] {
            basic_stats.set_automatic(false);
            basic_stats.set_accuracy(0.98);
            basic_stats.set_damage(120);
            basic_stats.set_damage_delta(0.1);
            basic_stats.set_fire_rate(1);
            return true;
        });

        stats = basic_stats;
    }

    const WeaponStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    const WeaponStats &get_stats() const override
    {
        return stats;
    }

    cf::Texture get_texture() const override
    {
        return texture;
    }
};
