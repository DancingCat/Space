#include "Weapon.hpp"

#include "game/spaceship/parts/weapon/Blaster.hpp"
#include "game/spaceship/parts/weapon/Minigun.hpp"
#include "game/spaceship/parts/weapon/Railgun.hpp"


std::shared_ptr<Weapon> Weapon::create_blaster()
{
    return std::make_shared<Blaster>();
}


std::shared_ptr<Weapon> Weapon::create_minigun()
{
    return std::make_shared<Minigun>();
}


std::shared_ptr<Weapon> Weapon::create_railgun()
{
    return std::make_shared<Railgun>();
}
