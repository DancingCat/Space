#pragma once


/**
 * @brief Характеристики оружия
 *
 * Характеристики оружия. Не могут выходить за пределы минимальных и максимальных значений.
 *
 * Разброс урона вычисляется относительно среднего урона. При уроне 50:
 * - При разбросе 0 урон всгда будет 50
 * - При разбросе 0.5 урон будет [25; 75]
 * - При разбросе 1 урон будет [0; 100]
 *
 * Точность определяет максимальное относительное отклонение от базового разброса. При базовом
 * разбросе 45 градусов точность 0.5 даст максимально возможное отклонение в 22.5 градусов,
 * точность 0.1 даст разброс (1 - 0.1) * 45 = 0.9 * 45 = 40.5 градусов. Точность 1 - нет отклонения.
 *
 * Флаг автоматического оружия определяет режим стрельбы. Автоматическое стреляет пока игрок удерживает
 * кнопку стрельбы. Для стрельбы из не автоматического оружия необходимо нажимать кнопку стрельбы для кажлого
 * выстрела отдельно.
 */
class WeaponStats
{
public:
    static constexpr double min_damage = 0; ///< Минимально возможный урон

    static constexpr double min_damage_delta = 0; ///< Минимально возможный разброс урона
    static constexpr double max_damage_delta = 1; ///< Максимально возможный разброс урона

    static constexpr double min_accuracy = 0.1; ///< Минимально возможная точность
    static constexpr double max_accuracy = 1;   ///< Максимально возможная точность

    static constexpr double base_spread_angle = 0.7071; ///< Базовый максимальный разброс (радиан) - 45 градусов

    static constexpr double min_fire_rate = 0.1; ///< Минимально возможная скорострельность
    static constexpr double max_fire_rate = 20;  ///< Максимально возможная скорострельность

private:
    double damage = min_damage;
    double damage_delta = min_damage_delta;
    double accuracy = min_accuracy;
    double fire_rate = min_fire_rate;
    bool automatic = false;

    inline double max(double a, double b)
    {
        return a > b
                 ? a
                 : b;
    }

    inline double minmax(double val, double min, double max)
    {
        return val < min
                 ? min
             : val > max
                 ? max
                 : val;
    }

public:
    /**
     * @brief Характеристики оружия
     */
    WeaponStats() = default;

    /**
     * @brief Получить средний урон
     */
    inline double get_damage() const
    {
        return damage;
    }

    /**
     * @brief Установить средний урон
     */
    inline void set_damage(double value)
    {
        damage = max(value, min_damage);
    }

    /**
     * @brief Получить относительный разброс урона
     */
    inline double get_damage_delta() const
    {
        return damage_delta;
    }

    /**
     * @brief Установить относительный разброс урона
     */
    inline void set_damage_delta(double value)
    {
        damage_delta = minmax(value, min_damage_delta, max_damage_delta);
    }

    /**
     * @brief Получить точность
     */
    inline double get_accuracy() const
    {
        return accuracy;
    }

    /**
     * @brief Установить точность
     */
    inline void set_accuracy(double value)
    {
        accuracy = minmax(value, min_accuracy, max_accuracy);
    }

    /**
     * @brief Получить скорострельность
     */
    inline double get_fire_rate() const
    {
        return fire_rate;
    }

    /**
     * @brief Установить скорострельность
     */
    inline void set_fire_rate(double value)
    {
        fire_rate = minmax(value, min_fire_rate, max_fire_rate);
    }

    /**
     * @brief Является ли оружие автоматическим
     */
    inline bool is_automatic() const
    {
        return automatic;
    }

    /**
     * @brief Установить флаг автоматического оружия
     */
    inline void set_automatic(bool value)
    {
        automatic = value;
    }
};
