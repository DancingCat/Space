#pragma once

#include <memory>
#include <string>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/parts/weapon/WeaponStats.hpp"


/**
 * @brief Вооружение корабля
 */
class Weapon
{
    std::string name;

public:
    /**
     * @brief Вооружение корабля
     *
     * @param name Название
     */
    inline Weapon(std::string name)
        : name(std::move(name))
    {
    }

    virtual ~Weapon() = default;

    Weapon(Weapon &) = delete;
    Weapon(Weapon &&) = delete;
    Weapon &operator=(Weapon &) = delete;
    Weapon &operator=(Weapon &&) = delete;

    /**
     * @brief Создать умолчательный объект
     *
     * Умолчательный объект - объект, который установлен на немодифицированном корабле
     */
    static inline std::shared_ptr<Weapon> create_default()
    {
        return create_blaster();
    }

    /**
     * @brief Получить название
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Получить базовые характеристики
     */
    virtual const WeaponStats &get_basic_stats() const = 0;

    /**
     * @brief Получить характеристики
     */
    virtual const WeaponStats &get_stats() const = 0;

    /**
     * @brief Получить текстуру
     */
    virtual cf::Texture get_texture() const = 0;

    /**
     * @brief Создать Бластер
     *
     * Бластер - автоматическое оружие со сбалансированной точностью и уроном
     */
    static std::shared_ptr<Weapon> create_blaster();

    /**
     * @brief Создать Миниган
     *
     * Миниган - скорострельное оружие с невысоким уроном не сниженной точностью
     */
    static std::shared_ptr<Weapon> create_minigun();

    /**
     * @brief Создать Рельсу
     *
     * Реальса - полу-автоматическое оружие с низкой скорострельностью, но высокими уроном и точностью
     */
    static std::shared_ptr<Weapon> create_railgun();
};
