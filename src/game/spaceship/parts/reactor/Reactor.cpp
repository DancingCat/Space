#include "Reactor.hpp"

#include "game/spaceship/parts/reactor/BlueReactor.hpp"
#include "game/spaceship/parts/reactor/GreenReactor.hpp"
#include "game/spaceship/parts/reactor/RedReactor.hpp"
#include "game/spaceship/parts/reactor/WhiteReactor.hpp"


std::shared_ptr<Reactor> Reactor::create_white()
{
    return std::make_shared<WhiteReactor>();
}


std::shared_ptr<Reactor> Reactor::create_red()
{
    return std::make_shared<RedReactor>();
}


std::shared_ptr<Reactor> Reactor::create_green()
{
    return std::make_shared<GreenReactor>();
}


std::shared_ptr<Reactor> Reactor::create_blue()
{
    return std::make_shared<BlueReactor>();
}
