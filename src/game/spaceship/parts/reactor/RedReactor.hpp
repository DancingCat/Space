#pragma once

#include "game/spaceship/parts/reactor/Reactor.hpp"


/**
 * @brief Красный реактор
 *
 * Красный реактор увеличивает урон ценой ускорения и щита
 */
class RedReactor : public Reactor
{
    static constexpr auto name = "Красный реактор";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .shield = 0,
                     .thrust = 0   },
        .relative = { .shield = 1,
                     .thrust = 1,
                     .damage = 1,
                     .fire_rate = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline RedReactor()
        : Reactor(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/reactor_red.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
