#pragma once

#include "game/spaceship/parts/reactor/Reactor.hpp"


/**
 * @brief Белый реактор
 *
 * Белый реактор равномерно распоеледяет энергию
 */
class WhiteReactor : public Reactor
{
    static constexpr auto name = "Белый реактор";
    static constexpr PartStats basic_stats = {
        .absolute = {.mass = 0,
                     .shield = 0,
                     .thrust = 0   },
        .relative = { .shield = 1,
                     .thrust = 1,
                     .damage = 1,
                     .fire_rate = 1}
    };

    cf::Texture texture;
    PartStats stats;

public:
    inline WhiteReactor()
        : Reactor(name)
        , texture(cf::Texture::load_from_file("assets/player/parts/reactor_white.png"))
        , stats(basic_stats)
    {
    }

    inline const PartStats &get_basic_stats() const override
    {
        return basic_stats;
    }

    inline const PartStats &get_stats() const override
    {
        return stats;
    }

    inline cf::Texture get_texture() const override
    {
        return texture;
    }
};
