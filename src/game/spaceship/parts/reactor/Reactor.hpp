#pragma once

#include <memory>
#include <string>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/PartStats.hpp"


/**
 * @brief Реактор
 *
 * Реактор отвечает за распределение энергии корабля, меняя бонус к ускорению, урону и щиту.
 */
class Reactor
{
    std::string name;

public:
    /**
     * @brief Создать умолчательный объект
     *
     * Умолчательный объект - объект, который установлен на немодифицированном корабле
     */
    static inline std::shared_ptr<Reactor> create_default()
    {
        return create_white();
    }

    /**
     * @brief Реактор
     *
     * @param name Название
     */
    inline Reactor(std::string name)
        : name(std::move(name))
    {
    }

    virtual ~Reactor() = default;

    Reactor(Reactor &) = delete;
    Reactor(Reactor &&) = delete;
    Reactor &operator=(Reactor &) = delete;
    Reactor &operator=(Reactor &&) = delete;

    /**
     * @brief Получить название
     */
    inline const std::string &get_name() const
    {
        return name;
    }

    /**
     * @brief Получить базовые характеристики
     */
    virtual const PartStats &get_basic_stats() const = 0;

    /**
     * @brief Получить характеристики
     */
    virtual const PartStats &get_stats() const = 0;

    /**
     * @brief Получить текстуру
     */
    virtual cf::Texture get_texture() const = 0;

    /**
     * @brief Создать Белый реактор
     *
     * Белый реактор равномерно распоеледяет энергию
     */
    static std::shared_ptr<Reactor> create_white();

    /**
     * @brief Создать Красный реактор
     *
     * Красный реактор увеличивает урон ценой ускорения и щита
     */
    static std::shared_ptr<Reactor> create_red();

    /**
     * @brief Создать Зеленый реактор
     *
     * Зеленый реактор увеличивает ускорение ценой урона и щита
     */
    static std::shared_ptr<Reactor> create_green();

    /**
     * @brief Создать Синий реактор
     *
     * Синий реактор усиливает щит ценой урона и ускорения
     */
    static std::shared_ptr<Reactor> create_blue();
};
