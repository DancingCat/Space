#pragma once

#include <type_traits>


/**
 * @brief Характеристики части корабля
 *
 * Характеристики обобщают все показатели части корабля. Для оружия являются дополнительными.
 */
struct PartStats
{
    struct Absolute
    {
        double mass = 0; ///< Масса

        double health = 0; ///< Прочность
        double shield = 0; ///< Щит
        double armor = 0;  ///< Броня

        double max_speed = 0; ///< Максимальная скорость
        double thrust = 0;    ///< Тяга

    } absolute; ///< Абсолютные характеристики

    struct Relative
    {
        double health = 1; ///< Прочность
        double shield = 1; ///< Щит
        double armor = 1;  ///< Броня

        double max_speed = 1; ///< Максимальная скорость
        double thrust = 1;    ///< Тяга

        double damage = 1;    ///< Урон (бонусный)
        double fire_rate = 1; ///< Скорострельность (бонусная)

    } relative; ///< Относительные бонусы

    /**
     * @brief Соединить показатели в новый объект
     */
    static constexpr PartStats merge(PartStats first, PartStats second)
    {
        return PartStats{
            .absolute = {.mass = first.absolute.mass + second.absolute.mass,
                         .health = first.absolute.health + second.absolute.health,
                         .shield = first.absolute.shield + second.absolute.shield,
                         .armor = first.absolute.armor + second.absolute.armor,
                         .max_speed = first.absolute.max_speed + second.absolute.max_speed,
                         .thrust = first.absolute.thrust + second.absolute.thrust         },

            .relative = { .health = first.relative.health * second.relative.health,
                         .shield = first.relative.shield * second.relative.shield,
                         .armor = first.relative.armor * second.relative.armor,
                         .max_speed = first.relative.max_speed * second.relative.max_speed,
                         .thrust = first.relative.thrust * second.relative.thrust,
                         .damage = first.relative.damage * second.relative.damage,
                         .fire_rate = first.relative.fire_rate * second.relative.fire_rate}
        };
    }

    /**
     * @brief Соединить показатели в новый объект
     */
    template <typename... Args>
    static constexpr PartStats merge(PartStats first, PartStats second, Args... args)
    {
        static_assert((std::is_same_v<Args, PartStats> and ...));

        return merge(merge(first, second), args...);
    }

    /**
     * @brief Привести показатели к абсолютному значению
     *
     * @note Приведение показателей необходимо для учета бонусов Relative
     * @note Приведение не учитывает бонусные характеристики, используемые для оружия
     */
    constexpr Absolute to_absolute_value() const
    {
        return Absolute{.mass = absolute.mass,
                        .health = absolute.health * relative.health,
                        .shield = absolute.shield * relative.shield,
                        .armor = absolute.armor * relative.armor,
                        .max_speed = absolute.max_speed * relative.max_speed,
                        .thrust = absolute.thrust * relative.thrust};
    }
};
