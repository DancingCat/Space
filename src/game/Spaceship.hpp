#pragma once

#include <utility>
#include <vector>

#include "cf_ui/Texture.hpp"

#include "game/spaceship/PartStats.hpp"
#include "game/spaceship/parts/weapon/WeaponStats.hpp"


/**
 * @brief Космический корабль
 */
class Spaceship
{
    PartStats::Absolute _stats = {};
    WeaponStats _weapon_stats = {};

    std::vector<cf::Texture> _texture_pack;
    cf::Texture _weapon_texture;

public:
    inline const std::vector<cf::Texture> &get_texture_pack() const
    {
        return _texture_pack;
    }

    inline cf::Texture get_weapon_texture() const
    {
        return _weapon_texture;
    }

private:
    Spaceship(PartStats::Absolute stats);

    friend class SpaceshipBuilder;
};
