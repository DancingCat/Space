#include "GameWidget.hpp"

#include <map>

#include "cf_ui/Button.hpp"
#include "cf_ui/KeyPressEvent.hpp"
#include "cf_ui/Painter.hpp"
#include "cf_ui/Surface.hpp"

#include "game/Spaceship.hpp"


namespace
{
    class PmButton : public cf::Button
    {
        std::string _text;

        std::map<cf::Button::State, cf::Texture> _render_cache;

    public:
        PmButton(std::string text, cf::SizeI size)
            : cf::Button(size)
            , _text(std::move(text))
        {
        }

    protected:
        void paint_event() override
        {
            cf::Painter painter(this);

            switch (state())
            {
            case cf::Button::Default:
            case cf::Button::Disabled:
            case cf::Button::Hovered:
            case cf::Button::Pressed:
                break;
            }
        }

        void update_render_cache()
        {
            _render_cache.clear();

            {
                cf::Surface surf(size());
                cf::Painter painter(surf);

                _render_cache[cf::Button::Default] = surf.make_texture();
            }

            {
                cf::Surface surf(size());
                cf::Painter painter(surf);

                _render_cache[cf::Button::Disabled] = surf.make_texture();
            }

            {
                cf::Surface surf(size());
                cf::Painter painter(surf);

                _render_cache[cf::Button::Hovered] = surf.make_texture();
            }

            {
                cf::Surface surf(size());
                cf::Painter painter(surf);

                _render_cache[cf::Button::Pressed] = surf.make_texture();
            }
        }
    };


    class PauseMenu : public cf::Widget
    {
    public:
        PauseMenu(const cf::SizeI &window_size)
            : cf::Widget(cf::PointI{}, window_size)
        {
            constexpr cf::SizeI button_size = {400, 75};

            auto continue_button = new PmButton("Продолжить", button_size);
            continue_button->clicked.connect(continue_clicked);
            add_child(continue_button);

            auto hangar_button = new PmButton("Выйти в ангар", button_size);
            hangar_button->clicked.connect(exit_to_hangar_clicked);
            add_child(hangar_button);
        }

        cf::Signal<> continue_clicked;
        cf::Signal<> exit_to_hangar_clicked;
    };
} // namespace


GameWidget::GameWidget(const cf::SizeI &window_size)
    : cf::Widget(cf::PointI{}, window_size)
    , _pause_menu(nullptr)
{
    PauseMenu *pm = new PauseMenu(window_size);
    _pause_menu = pm;
    add_child(_pause_menu);

    pm->continue_clicked.connect([this]() { set_paused(false); });
    pm->exit_to_hangar_clicked.connect(this->exit_to_hangar_clicked);

    set_paused(false);
}


void GameWidget::set_paused(bool value)
{
    _paused = value;
    _pause_menu->set_visible(_paused);
}


void GameWidget::key_press_event(cf::KeyPressEvent *event)
{
    if (event->pressed() and event->key_code() == SDL_SCANCODE_ESCAPE)
    {
        set_paused(not _paused);
    }
}


void GameWidget::paint_event()
{
    cf::Painter painter(this);

    painter.set_color({0x00, 0x00, 0x00, 0xff});
    painter.fill_rect(cf::RectI{cf::PointI{}, size()});

    if (_player != nullptr)
    {
        cf::RectI rect = cf::RectI::by_center(cf::PointI{250, 250}, cf::SizeI{50, 50});

        painter.draw_texture(_player->get_weapon_texture(), rect);

        for (auto &&item : _player->get_texture_pack())
        {
            painter.draw_texture(item, rect);
        }

        // painter.set_color(cf::Color{0xff, 0x00, 0x00, 0xff});
        // painter.draw_rect(rect);
    }
}


void GameWidget::init()
{
    set_paused(false);
}
